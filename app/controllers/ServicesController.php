<?php

namespace app\controllers;

use Props\NotFoundException;
use yii\easyii\modules\article\api\Article;
use yii\web\Controller;

class ServicesController extends Controller
{

    public function actionView($slug)
    {
        $article = Article::get($slug);

        if(empty($article)) throw new NotFoundException();

        return $this->render('view', ['article' => $article]);
    }
}