<?php
use yii\easyii\modules\page\api\Page;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Url;
use yii\easyii\modules\carousel\api\Carousel;
use yii\easyii\modules\guestbook\api\Guestbook;
use yii\easyii\modules\article\api\Article;

$page = Page::get('page-index');
$this->title = $page->seo('title', $page->model->title);

?>
<section class="section-banner">
    <div class="container">
        <?= Carousel::widget(1200, 400) ?>
    </div>
</section>
<section class="section-price">
    <div class="container">
        <div class="price">
            <div class="price__title"><?=Text::get('index-phrase-title') ?></div>
            <div class="price__content"><?=Text::get('index-phrase-text') ?></div>
        </div>
    </div>
</section>
<section class="section-text">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="company">
                    <h1 class="company__title" style="margin-top: 0px; padding-bottom: 0px;margin-bottom: 0px;"><?=$page->seo('h1', $page->title) ?></h1>
                    <div class="company__content">
                        <?=$page->text ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="company__title"><?=Text::get('index-news') ?></div>
                <div class="company__content">
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script><!-- VK Widget -->
                    <div id="vk_groups" style="margin-left: 23px;">&nbsp;</div>
                    <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 2, width: "auto", height: "310"}, 62450569);
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="advantages">
    <div class="container">

        <div class="row" style="margin-top: 50px;">
            <div class="benefit"
                 style="height:300px;background-image:url(/images/1.png);">
                <p class="bentitle"><?=Text::get('index-adv-title-1') ?></p>

                <p class="bentitle2"><?=Text::get('index-adv-text-1') ?></p>
            </div>

            <div class="benefit"
                 style="height:300px;background-image:url(/images/2.png);">
                <p class="bentitle"><?=Text::get('index-adv-title-2') ?></p>

                <p class="bentitle2"><?=Text::get('index-adv-text-2') ?></p>
            </div>

            <div class="benefit"
                 style="height:300px;background-image:url(/images/3.png);">
                <p class="bentitle"><?=Text::get('index-adv-title-3') ?></p>

                <p class="bentitle2"><?=Text::get('index-adv-text-3') ?></p>
            </div>
        </div>
        <div class="row">

            <div class="benefit"
                 style="height:300px;background-image:url(/images/4.png);">
                <p class="bentitle"><?=Text::get('index-adv-title-4') ?></p>

                <p class="bentitle2"><?=Text::get('index-adv-text-4') ?></p>
            </div>

            <div class="benefit"
                 style="height:300px;background-image:url(/images/5.png);">
                <p class="bentitle"><?=Text::get('index-adv-title-5') ?></p>

                <p class="bentitle2"><?=Text::get('index-adv-text-5') ?></p>
            </div>

            <div class="benefit"
                 style="height:300px;background-image:url(/images/6.png);">
                <p class="bentitle"><?=Text::get('index-adv-title-6') ?></p>

                <p class="bentitle2"><?=Text::get('index-adv-text-6') ?></p>
            </div>
        </div>
    </div>
</section>
<section class="info">
    <div class="container">
        <div class="company__title"><?=Text::get('index-info') ?></div>
        
        <? 
        	$items = [];
        	$x = 0;
        	foreach (Article::cat('info')->items() as $i => $item ){
        		$items[$x][] = $item;
        		if($i % 2) $x++;
        		
        	}
        ?>
		
		<? foreach($items as $item):?>
		<div class="row">
            <div class="col-md-6">
                <div class="info__item">
                    <a class="info__item__title"
                       href="/info/<?=$item[0]->slug?>"><?=$item[0]->title?></a>
                    <p class="info__item__content"><?=$item[0]->short?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="info__item">
                    <a class="info__item__title"
                       href="/info/<?=$item[1]->slug?>"><?=$item[1]->title?></a>
                    <p class="info__item__content"><?=$item[1]->short?></p>
                </div>
            </div>
        </div>
		<?endforeach;?>
		
    </div>
</section>


<section class="main-reviews main-section">
    <div class="container">

        <div style="margin-top: 20px;" class="company__title"><?=Text::get('index-feedback') ?></div>
        <div class="main-reviews__list row">
            <div class="owl-carousel owl-theme owl-loaded owl-drag">
            <?php foreach(Guestbook::items() as $item) : ?>
                <div class="main-reviews__box">
                    <div class="review-item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
                        <meta itemprop="name" content="<?= $item->model->name ?>">
                        <div class="review-item__preview">
                            <p class="review-item__author" itemprop="author"><?= $item->model->name ?></p>
<!--                            <p class="review-item__date">--><?//= date('d.m.Y H:i', $item->time) ?><!--</p>-->
                        </div>
                        <div class="review-item__main">
                            <div class="i-wai" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
                                <meta itemprop="worstRating" content="1">
                                <meta itemprop="ratingValue" content="5">
                                <meta itemprop="bestRating" content="5">
                            </div>
                            <div class="review-item__desc" itemprop="description">
                                <p><?= preg_replace('/(^.{200}[^\s]*).*/u','$1..',$item->model->text) ?></p>
                                <p><a class="review-item__btn-detail js-fancybox" href="#reviews-<?= $item->model->guestbook_id?>" rel="main-reviews">Читать полностью</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="review-item review-item_detail" id="reviews-<?= $item->model->guestbook_id?>" style="display: none;">
                        <div class="review-item__preview">
                            <p class="review-item__author"><?= $item->name ?></p>
                        </div>
                        <div class="review-item__main">
                            <div class="review-item__desc">
                                <p><?= $item->model->text ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
        <?php if(Yii::$app->request->get(Guestbook::SENT_VAR)) : ?>
            <h4 class="text-success" style="text-align: center;"><i class="glyphicon glyphicon-ok"></i> После проверки администрции отзыв появиться на сайте</h4>
            <script> $('html,body').animate({ scrollTop: $(".main-reviews").offset().top - 150 }, 1000); </script>
        <?php else :?>
            <div class="main-reviews__btns">
                <a class="main-reviews__btn-write btn-add-review js-fancybox" href="#form-review">Написать отзыв</a>
            </div>
        <?php endif; ?>
    </div>
</section>

<script>
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText:['предыдущие','следующие'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })
</script>

<div style="display:none; min-width: 300px;" id="form-review">
    <h4>Оставить отзыв</h4>
    <?= Guestbook::form() ?>
</div>