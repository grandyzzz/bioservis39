<?php
use yii\easyii\helpers\Image;
use yii\easyii\modules\article\api\Article;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>

    <div id="wrapper">
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="social">
                        <div class="social__networks">
                            <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                            <div class="yashare-auto-init" data-yasharel10n="ru" data-yasharequickservices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus" data-yasharetheme="counter">&nbsp;</div>
                        </div>
                        <div class="social__number">
                            <a href="tel:+74012905990?call" title="Позвонить через skype">
                                <img alt="" height="41" src="<?=Image::thumb('/app/media/images/headermob.png')?>" width="34" />
                                Наш телефон: <span><?=Text::get('header-phone') ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-down">

                <?
                $items = [];
                foreach (Article::cat('menu')->items() as $item ){
                    $items[] = [
                        'active' => Yii::$app->request->get('slug') == $item->slug,
                        'url' => Url::to('/services/'.$item->slug),
                        'label' => $item->model->title
                    ];
                }
                $items[] = [
                    'active' => Yii::$app->controller->getRoute() == 'contacts/index',
                    'url' => Url::to('/contacts'),
                    'label' => 'Контакты'
                ];

                NavBar::begin(['brandLabel' => Html::img(Image::thumb('/app/media/images/logo.jpg'))]);
                echo Nav::widget([
                      'items' => $items,
                     'options' => ['class' => 'menu'],
                 ]);
                NavBar::end();





                ?>


<!--                <nav class="navbar navbar-default">-->
<!--                    <div class="container">-->
<!--                        <div class="flex-container">-->
<!--                            <div class="navbar-header">-->
<!--                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">-->
<!--                                    <span class="sr-only">Toggle navigation</span>-->
<!--                                    <span class="icon-bar"></span>-->
<!--                                    <span class="icon-bar"></span>-->
<!--                                    <span class="icon-bar"></span>-->
<!--                                </button>-->
<!--                                <a class="navbar-brand" href="/">-->
<!--                                    <img alt="Brand" src="--><?//=Image::thumb('/app/media/images/logo.jpg')?><!--">-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <ul class="menu">-->
<!--                                --><?// foreach (Article::cat('menu')->items() as $item ):?>
<!--                                <li><a href="--><?//=Url::to('/services/'.$item->slug)?><!--"><span>--><?//=$item->title?><!--</span></a></li>-->
<!--                                --><?// endforeach;?>
<!--                                <li><a href="/contacts"><span>Контакты</span></a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </nav>-->
            </div>
        </header>

        <main>
            <?= $content ?>
        </main>

        <footer>
            <div class="container">
                <div class="footer">
                    <div class="footer__content">
                        <ul>
                            <? foreach (Article::cat('menu')->items() as $item): ?>
                            <li>
                                <a href="<?=Url::to('/services/'.$item->slug)?>"><?=$item->title?></a>
                            </li>
                            <? endforeach;?>
                            <li><a href="/contacts">Контакты</a></li>
                        </ul>
                        <p style="font-size: 12px">Copyright &copy; 2014-<?=date('Y');?> ООО &quot;Био-сервис&quot;</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

<!-- Модуль: вставка кода -->
<? foreach (\grozzzny\editable\models\Editable::findAll(['status' => \grozzzny\editable\models\Editable::STATUS_ON]) as $editable) echo $editable->code.PHP_EOL; ?>

<?php $this->endContent(); ?>