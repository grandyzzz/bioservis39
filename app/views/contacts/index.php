<?php
use yii\easyii\modules\feedback\api\Feedback;
use yii\easyii\modules\page\api\Page;
use app\widgets\google_map\GoogleMap;
use yii\easyii\models\Setting;

$page = Page::get('page-contact');

$this->title = $page->seo('title', $page->model->title);
$this->params['breadcrumbs'][] = $page->model->title;
?>

<div class="container margin_header">

    <?= \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])?>

    <h1>
        <?= $page->seo('h1', $page->title) ?>
    </h1>


    <div class="row">
        <div class="col-md-8">
            <?= $page->text ?>

            <div style="margin-top: 20px;">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3a1edd8a6cd7673f7a743355dc3f77249413df1f5326417cbcbdab379a61ca58&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>

        </div>
        <div class="col-md-4">
            <?php if(Yii::$app->request->get(Feedback::SENT_VAR)) : ?>
                <h4 class="text-success" style="margin-bottom: 20px;"><i class="glyphicon glyphicon-ok"></i> Сообщение успешно отправлено</h4>
            <?php else : ?>
                <h4 style="margin-bottom: 20px;"><i class="glyphicon glyphicon-envelope"></i> Форма обратной связи</h4>
                <div class="well well-sm">
                    <?= Feedback::form() ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>