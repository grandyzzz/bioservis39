<?php
use yii\easyii\modules\feedback\api\Feedback;
use yii\easyii\modules\page\api\Page;
use app\widgets\google_map\GoogleMap;
use yii\easyii\models\Setting;
use yii\helpers\Html;
use yii\easyii\modules\article\api\Article;

$this->title = $article->seo('title', $article->model->title);
$this->params['breadcrumbs'][] = $article->model->title;
?>

<div class="container">
    <?= \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])?>
</div>

<!--<section class="section-banner" style="margin-bottom: 20px;">-->
<!--    <div class="container">-->
<!--    	--><?// if(!empty($article->image)):?>
<!--        	--><?//=Html::img($article->thumb(1200, 200), ['style' => 'width:100%']);?>
<!--        --><?// endif;?>
<!--    </div>-->
<!--</section>-->

<div class="container margin_header main-content">

    <h1>
        <?= $article->seo('h1', $article->title) ?>
    </h1>


    <div class="row">
        <div class="col-md-12">
            <?= $article->text ?>
        </div>
    </div>
    
    <?php if(count($article->photos)) : ?>
        <div>
            <?php foreach($article->photos as $photo) : ?>
                <?= $photo->box(100, 100) ?>
            <?php endforeach;?>
            <?php Article::plugin() ?>
        </div>
        <br/>
    <?php endif; ?>

</div>