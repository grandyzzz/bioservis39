<?php
namespace app\assets;

use yii\web\View;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/styles.css',
        'OwlCarousel2-2.2.1/dist/assets/owl.carousel.css',
        'OwlCarousel2-2.2.1/dist/assets/owl.theme.default.css',
    ];
    public $js = [
        'js/scripts.js',
        'https://use.fontawesome.com/05e992c469.js',
        'OwlCarousel2-2.2.1/dist/owl.carousel.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\easyii\assets\FancyboxAsset',
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
}
