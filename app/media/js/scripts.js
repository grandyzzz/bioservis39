$(function(){
    $(document).on('ready', function () {
        $('.js-fancybox').fancybox({
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            overlayColor: '#000',
            overlayOpacity: '0.8'
        });
    });
});