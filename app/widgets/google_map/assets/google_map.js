/**********************
 - Google Maps - grozzzny
 **********************/
var googleMaps = function(id) {

    var t = this;
    var config;
    var markers;
    var container;
    var style= [];
    var map_options;
    var map;
    var is_mobile;

    //Метод формирования стилей карты
    this.setStyle = function()
    {
        //Цветовой фон карты
        var main_color = t.config.color;

        //Насыщенность и яркость
        var saturation_value = -10;
        var brightness_value = 0;

        //Формируем параметры стиля карты
        t.style= [
            {//Set saturation for the labels on the map
                elementType:"labels",
                stylers:[
                    {saturation:saturation_value},
                ]
            },

            {//Poi stands for point of interest - don't show these labels on the map
                featureType:"poi",
                elementType:"labels",
                stylers:[
                    {visibility:"off"},
                ]
            },

            {//Hide highways labels on the map
                featureType:'road.highway',
                elementType:'labels',
                stylers:[
                    {visibility:"off"},
                ]
            },

            {//Hide local road labels on the map
                featureType:"road.local",
                elementType:"labels.icon",
                stylers:[
                    {visibility:"off"},
                ]
            },

            {//Hide arterial road labels on the map
                featureType:"road.arterial",
                elementType:"labels.icon",
                stylers:[
                    {visibility:"off"},
                ]
            },

            {//Hide road labels on the map
                featureType:"road",
                elementType:"geometry.stroke",
                stylers:[
                    {visibility:"off"},
                ]
            },

            {//Style different elements on the map
                featureType:"transit",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"poi",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"poi.government",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"poi.sport_complex",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"poi.attraction",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"poi.business",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"transit",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"transit.station",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"landscape",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"road",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"road.highway",
                elementType:"geometry.fill",
                stylers:[
                    {hue:main_color},
                    {visibility:"on"},
                    {lightness:brightness_value},
                    {saturation:saturation_value},
                ]
            },

            {
                featureType:"water",
                elementType:"geometry",
                stylers:[
                    {color:'#03284a'},
                ]
            }
        ];
    };

    //Метод установки параметров для карты
    this.setOptions = function()
    {
        t.map_options = {
            center:new google.maps.LatLng(t.config.latitude, t.config.longitude),
            zoom:t.config.zoom,
            panControl:false,
            zoomControl:true,
            mapTypeControl:false,
            streetViewControl:false,
            mapTypeId:google.maps.MapTypeId.ROADMAP,
            scrollwheel:false,
            styles:t.style
        };
    };

    //Метод устанавливает точки на карте
    this.addMarkers = function ()
    {

        var gooddeOverlay = function(position, content,  map) {
            this.center = position;
            this.map = map;
            this.content = content;

            this.setMap(map);
        };
        gooddeOverlay.prototype = new google.maps.OverlayView();
        gooddeOverlay.prototype.onAdd = function(){
            var panes = this.getPanes();
            panes.overlayMouseTarget.appendChild(this.content);
        };
        gooddeOverlay.prototype.draw = function(){
            var overlayProjection = this.getProjection();
            var position = overlayProjection.fromLatLngToDivPixel(this.center);
            var ths = $(this.content);
            var text = $(".text_holder", ths);

            ths.css({
                left: position.x,
                top: position.y
            }).show();
            ths.off(t.is_mobile ? 'touchstart' : 'click');
            ths.on(t.is_mobile ? 'touchstart' : 'click', function(e){
                e.stopPropagation();
                e.preventDefault();
                if(!(ths.hasClass('active'))){
                    $("#google-container .marker_holder").removeClass("active");

                    ths.toggleClass('active');
                    $("#google-container .text_holder").fadeOut();
                    text.fadeIn();
                }else{
                    $("#google-container .marker_holder").removeClass("active");
                    $("#google-container .text_holder").fadeOut();
                }
            });

        };
        gooddeOverlay.prototype.onRemove = function(){
            $(this.content).remove();
        };

        $.each(t.markers, function () {

            if(this.primary == true){
                new google.maps.Marker({
                    position:new google.maps.LatLng(this.latitude, this.longitude),
                    map: t.map,
                    title: this.title,
                    visible: true,
                    icon: this.icon
                });
                return;
            }

            var overlayContent = $(
                "<div class='marker_holder'>"+
                "<div class='marker_wrapper'>"+
                "<img src='"+this.icon+"' />"+
                "<span class='close'></span>"+
                "</div>"+
                "<div class='text_holder'>"+
                "<span class='text' style='background-color: "+this.color+";'>"+this.title+"</span>"+
                "</div>"+
                "</div>"
            )[0];


            new gooddeOverlay(
                new google.maps.LatLng(this.latitude, this.longitude),
                overlayContent,
                t.map
            );



            // objects[index].overlay = new gooddeOverlay(
            //     new google.maps.LatLng(element.position[0], element.position[1]),
            //     overlayContent,
            //     location_map
            // );


            // var infowindow = new google.maps.InfoWindow({content: marker.template});
            // //Устанавливаем событие при клике на балун
            // google.maps.event.addListener(marker, 'click', (function (infowindow){return function (e){
            //     infowindow.open(t.map, marker);
            // }
            // })(infowindow));
            //
            // if (this.open_marker) infowindow.open(t.map, marker);
        });
    };

    //Метод присвоения событий к карте
    this.setEvent = function ()
    {
        google.maps.event.addDomListener(window, "resize", function() {
            var center = t.map.getCenter();
            google.maps.event.trigger(t.map, "resize");
            t.map.setCenter(center);
        });
    };

    //Метод инициализирует кнопки зума на карте
    this.setZoomControl = function ()
    {
        function customZoomControl(controlDiv, map) {
            //Grap the zoom elements from the DOM and insert them in the map
            var controlUIzoomIn = document.getElementById(t.config.zoom_in_id),
                controlUIzoomOut = document.getElementById(t.config.zoom_out_id);

            controlDiv.appendChild(controlUIzoomIn);
            controlDiv.appendChild(controlUIzoomOut);

            //Setup the click event listeners and zoom-in or out according to the clicked element
            google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
                map.setZoom(map.getZoom()+1);
            });

            google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
                map.setZoom(map.getZoom()-1);
            });
        }

        var zoomControlDiv = document.createElement('div');
        //var zoomControl = new customZoomControl(zoomControlDiv, t.map);

        //Insert the zoom div on the top left of the map
        t.map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
    };

    // Конструктор класса
    var __construct = function()
    {

        (function(){
            var a = navigator.userAgent||navigator.vendor||window.opera;
            t.is_mobile = (/android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(ad|hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)));
        })();


        t.container = document.getElementById(id);
        t.config = $('#'+id).data('config');
        t.markers = $('#'+id).data('markers');

        //Установим стиль карты
      //  t.setStyle();

        //Соберем конфигурацию
        t.setOptions();

        //Инициализируем карту
        t.map = new google.maps.Map(t.container, t.map_options);

        //Установим точки на карте
        t.addMarkers();

        //Установим события карты
        t.setEvent();

        //Установим кнопки зума
        t.setZoomControl();
    }();

};