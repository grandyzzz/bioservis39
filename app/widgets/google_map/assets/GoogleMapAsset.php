<?php

namespace app\widgets\google_map\assets;

use yii\web\AssetBundle;
use yii\web\View;

class GoogleMapAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/google_map/assets';

    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyCtX2Fg1fOnnX1Pu1n1lXvmb303Q_hn2iI',
        'google_map.js',
    ];

    public $css = [
        'google_map.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];
}