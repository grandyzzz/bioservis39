<?php
namespace app\widgets\google_map;

use yii\base\Widget;
use app\widgets\google_map\assets\GoogleMapAsset;

class GoogleMap extends Widget
{

    public $id;
    public $config;
    public $markers;
    public $options = [];

    /**
     * Инициализация виджита
     */
    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    /**
     * Метод run выводит HTML
     * @return string
     */
    public function run()
    {

        $config = json_encode($this->config, JSON_UNESCAPED_UNICODE);
        $markers = json_encode($this->markers, JSON_UNESCAPED_UNICODE);

        $options = $this->options
            + ['id' => $this->id]
            + ['data-config' => $config]
            + ['data-markers' => $markers];

        return $this->render('index', [
            'id' => $this->id,
            'options' => $options
        ]);
    }

    /**
     * Регистрация assets (Файлы относящие к виджету CSS, JS)
     */
    public function registerAssets()
    {
        $view = $this->getView();
        GoogleMapAsset::register($view);
    }



}