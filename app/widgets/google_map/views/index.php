<?php
use yii\web\View;
use yii\helpers\Html;

echo Html::tag('div','', $options);

$js_variable = 'map_'.preg_replace('/[^a-z]/i','_',$id);

$script = <<< JS
    $(document).ready(function() {
        var $js_variable = new googleMaps('$id');
    });
JS;
$this->registerJs($script, View::POS_READY);
?>